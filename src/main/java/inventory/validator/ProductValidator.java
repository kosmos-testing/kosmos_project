package inventory.validator;

import inventory.model.Product;

public class ProductValidator implements Validator<Product>{
    @Override
    public String validate(Product entity) {
        String errorMessage = "";
        double sumOfParts = 0.00;
//        entity.getAssociatedParts().stream().reduce(0.0,)
        for (int i = 0; i < entity.getAssociatedParts().size(); i++) {
            sumOfParts += entity.getAssociatedParts().get(i).getPrice();
        }

        if (entity.getMin() < 0) {
            errorMessage += "The inventory level must be greater than 0. ";
        }
        if (entity.getPrice() < 0.01) {
            errorMessage += "The price must be greater than $0. ";
        }
        if (entity.getMin() > entity.getMax()) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if(entity.getNoInStock() < entity.getMin()) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if(entity.getNoInStock() > entity.getMax()) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }

        if (sumOfParts > entity.getPrice()) {
            errorMessage += "Product price must be greater than cost of parts. ";
        }

        if(entity.getName().length() < 3 || entity.getName().length() > 30){
            errorMessage += "Name length has to be between 3 and 30";
        }
        return errorMessage;
    }
}
