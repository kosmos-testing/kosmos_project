package inventory.validator;

public interface Validator<T> {
    String validate(T entity) ;
}
