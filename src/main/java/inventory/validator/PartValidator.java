package inventory.validator;

import inventory.model.Part;

public class PartValidator implements Validator<Part>{
    @Override
    public String validate(Part entity) {
        String errorMessage = "";
        if(entity.getName().equals("")) {
            errorMessage += "A name has not been entered. ";
        }
        if(entity.getPrice() < 0.01) {
            errorMessage += "The price must be greater than 0. ";
        }
        if(entity.getNoInStock() < 1) {
            errorMessage += "Inventory level must be greater than 0. ";
        }
        if(entity.getMin() > entity.getMax()) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if(entity.getNoInStock() < entity.getMin()) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if(entity.getNoInStock() > entity.getMax()) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }
        return errorMessage;
    }
}
