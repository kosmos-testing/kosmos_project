package inventory.service;

import inventory.exceptions.ServiceException;
import inventory.model.*;
import inventory.repository.InventoryRepository;
import inventory.validator.Validator;
import javafx.collections.ObservableList;

public class InventoryService {

    private InventoryRepository repo;
    private Validator<Part> partValidator;
    private Validator<Product>  productValidator;


    public InventoryService(InventoryRepository repo, Validator<Part> partValidator, Validator<Product> productValidator) {
        this.repo = repo;
        this.partValidator = partValidator;
        this.productValidator = productValidator;
    }

    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue) throws ServiceException {
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        String errorMessage = partValidator.validate(inhousePart);
        if(errorMessage.length() >0){
            throw new ServiceException(errorMessage);
        }
        repo.addPart(inhousePart);
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue) throws ServiceException {
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        String errorMessage = partValidator.validate(outsourcedPart);
        if(errorMessage.length() >0){
            throw new ServiceException(errorMessage);
        }
        repo.addPart(outsourcedPart);
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts) throws ServiceException {
        Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
        String errorMessage = productValidator.validate(product);
        if(errorMessage.length() >0){
            throw new ServiceException(errorMessage);
        }
        repo.addProduct(product);
    }

    public ObservableList<Part> getAllParts() {
        return repo.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    public Part lookupPart(String search) {
        return repo.lookupPart(search);
    }

    public Product lookupProduct(String search) {
        return repo.lookupProduct(search);
    }

    public void updateInhousePart(int partIndex, int partId, String name, double price, int inStock, int min, int max, int partDynamicValue) throws ServiceException {
        InhousePart inhousePart = new InhousePart(partId, name, price, inStock, min, max, partDynamicValue);
        String errorMessage = partValidator.validate(inhousePart);
        if(errorMessage.length() >0){
            throw new ServiceException(errorMessage);
        }
        repo.updatePart(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, int partId, String name, double price, int inStock, int min, int max, String partDynamicValue) throws ServiceException {
        OutsourcedPart outsourcedPart = new OutsourcedPart(partId, name, price, inStock, min, max, partDynamicValue);
        String errorMessage = partValidator.validate(outsourcedPart);
        if(errorMessage.length() >0){
            throw new ServiceException(errorMessage);
        }
        repo.updatePart(partIndex, outsourcedPart);
    }

    public void updateProduct(int productIndex, int productId, String name, double price, int inStock, int min, int max, ObservableList<Part> addParts) throws ServiceException {
        Product product = new Product(productId, name, price, inStock, min, max, addParts);
        String errorMessage = productValidator.validate(product);
        if(errorMessage.length() >0){
            throw new ServiceException(errorMessage);
        }
        repo.updateProduct(productIndex, product);
    }

    public void deletePart(Part part){
        repo.deletePart(part);
    }

    public void deleteProduct(Product product){
        repo.deleteProduct(product);
    }

}
