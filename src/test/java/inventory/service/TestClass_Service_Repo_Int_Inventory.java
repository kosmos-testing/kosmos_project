package inventory.service;

import inventory.exceptions.ServiceException;
import inventory.repository.InventoryRepository;
import inventory.validator.PartValidator;
import inventory.validator.ProductValidator;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestClass_Service_Repo_Int_Inventory {
    private String filename = "testFile.txt";
    InventoryService service;

    @BeforeEach
    void setUp() {
        PartValidator partValidator = new PartValidator();
        ProductValidator productValidator = new ProductValidator();
        InventoryRepository repo= new InventoryRepository(filename);
        service = new InventoryService(repo,partValidator,productValidator);
    }

    @DisplayName("Test add product valid")
    @Test
    void test_addProduct_Valid() {
        Integer countBeforeAdding = service.getAllProducts().size();

        assertDoesNotThrow(() -> service.addProduct("Electric Car", 273001.99, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();

        assertEquals(countAfterAdding - countBeforeAdding, 1);
    }

    @Test
    @DisplayName("Test add product nonvalid where price very negative and name is too short")
    void test_addProduct_nonvalid_nameTooShort_priceNegativeNameTooShort(){
        Integer countBeforeAdding = service.getAllProducts().size();

        ServiceException exceptionThrown = assertThrows(ServiceException.class,() -> service.addProduct("AA", -700, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();

        assertEquals(countAfterAdding , countBeforeAdding);

        assertEquals(exceptionThrown.getMessage().trim(), "The price must be greater than $0. Product price must be greater than cost of parts. Name length has to be between 3 and 30".trim());
    }

    @Test
    @DisplayName("Test add product nonvalid name too long and negative price")
    void test_addProduct_nonvalid_nameTooLong_priceNegative(){
        Integer countBeforeAdding = service.getAllProducts().size();

        ServiceException exceptionThrown = assertThrows(ServiceException.class,() -> service.addProduct("1234567890123456789012345678901", -1, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();

        assertEquals(countAfterAdding , countBeforeAdding);

        assertEquals(exceptionThrown.getMessage().trim(), "The price must be greater than $0. Product price must be greater than cost of parts. Name length has to be between 3 and 30".trim());
    }

    @AfterEach
    void tearDown() {
        try {
            ClassLoader classLoader = InventoryRepository.class.getClassLoader();
            File file = new File(classLoader.getResource(filename).getFile());
            new PrintWriter(file).close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
