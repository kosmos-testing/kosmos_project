package inventory.service;

import inventory.exceptions.ServiceException;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.validator.PartValidator;
import inventory.validator.ProductValidator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.security.Provider;
import java.util.ArrayList;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

class TestClass_AddProduct {

    private String filename = "testFile.txt";
    InventoryService service;

    @BeforeEach
    void setUp() {
        PartValidator partValidator = new PartValidator();
        ProductValidator productValidator = new ProductValidator();
        InventoryRepository repo= new InventoryRepository(filename);
        service = new InventoryService(repo,partValidator,productValidator);
    }

    @DisplayName("Test add product ECP valid")
    @Tag("ECP")
    @RepeatedTest(5)
    void test_addProduct_ECP_Valid() {
        Integer countBeforeAdding = service.getAllProducts().size();

        assertDoesNotThrow(() -> service.addProduct("Electric Car", 273001.99, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();

        assertEquals(countAfterAdding - countBeforeAdding, 1);
    }

    @Test
    @DisplayName("Test add product ECP nonvalid where price very negative and name is too short")
    @Tag("ECP")
    void test_addProduct_ECP_nonvalid_nameTooShort_priceNegativeNameTooShort(){
        Integer countBeforeAdding = service.getAllProducts().size();

        ServiceException exceptionThrown = assertThrows(ServiceException.class,() -> service.addProduct("AA", -700, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();

        assertEquals(countAfterAdding , countBeforeAdding);

        assertEquals(exceptionThrown.getMessage().trim(), "The price must be greater than $0. Product price must be greater than cost of parts. Name length has to be between 3 and 30".trim());
    }

    @Test
    @DisplayName("Test add product ECP nonvalid name too long and negative price")
    @Tag("ECP")
    void test_addProduct_ECP_nonvalid_nameTooLong_priceNegative(){
        Integer countBeforeAdding = service.getAllProducts().size();

        ServiceException exceptionThrown = assertThrows(ServiceException.class,() -> service.addProduct("1234567890123456789012345678901", -1, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();

        assertEquals(countAfterAdding , countBeforeAdding);

        assertEquals(exceptionThrown.getMessage().trim(), "The price must be greater than $0. Product price must be greater than cost of parts. Name length has to be between 3 and 30".trim());
    }

    @ParameterizedTest
    @ValueSource(strings =  {"MAI", "ABCD"})
    @DisplayName("Test add product BVA valid name lengths lower boundaries")
    @Tag("BVA")
    void test_addProduct_BVA_valid_nameLengthLowerBoundaries(String name){
        Integer countBeforeAdding = service.getAllProducts().size();

        assertDoesNotThrow(() -> service.addProduct(name, 2.0, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();

        assertEquals(countAfterAdding - countBeforeAdding, 1);
    }

    @ParameterizedTest
    @ValueSource(strings =  { "12345678901234567890123456789", "123456789012345678901234567890"})
    @DisplayName("Test add product BVA valid name lengths upper boundaries")
    @Tag("BVA")
    void test_addProduct_BVA_valid_nameLengthUpperBoundaries(String name){
        Integer countBeforeAdding = service.getAllProducts().size();

        assertDoesNotThrow(() -> service.addProduct(name, 2.0, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();
        assertEquals(countAfterAdding - countBeforeAdding, 1);
    }


    @Test
    @DisplayName("Test add product BVA valid with price = 1")
    @Tag("BVA")
    void test_addProduct_BVA_valid_price1(){
        Integer countBeforeAdding = service.getAllProducts().size();

        assertDoesNotThrow(() -> service.addProduct("MAI", 1, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();
        assertEquals(countAfterAdding - countBeforeAdding, 1);
    }

    @TestFactory
    @DisplayName("Test add product BVA price maximum float")
    @Tag("BVA")
    Stream<DynamicTest> dynamicTests(){
           return Stream.of(Float.MAX_VALUE - 1, Float.MAX_VALUE).map((price) -> dynamicTest("Test: " + price, () -> {
               Integer countBeforeAdding = service.getAllProducts().size();

               assertDoesNotThrow(() -> service.addProduct("MAI", price, 2, 1, 5, FXCollections.observableArrayList()));
               Integer countAfterAdding = service.getAllProducts().size();
               assertEquals(countAfterAdding - countBeforeAdding, 1);
           }));
    }

    @AfterEach
    void tearDown() {
        try {
            ClassLoader classLoader = InventoryRepository.class.getClassLoader();
            File file = new File(classLoader.getResource(filename).getFile());
            new PrintWriter(file).close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}