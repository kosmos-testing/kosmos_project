package inventory.service;

import inventory.exceptions.ServiceException;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.validator.PartValidator;
import inventory.validator.ProductValidator;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.mockito.Mockito.*;

public class TestClass_Service_Unit {
    InventoryService service;
    PartValidator partValidator;
    ProductValidator productValidator;
    InventoryRepository repo;
    Product dummyProduct;

    @BeforeEach
    void setUp() {
        partValidator = new PartValidator();
        productValidator = new ProductValidator();
        repo= mock(InventoryRepository.class);
    }

    @DisplayName("Test add product")
    @Test
    void test_addProduct_Valid() {
        ProductValidator spiedValidator = spy(productValidator);
        service = new InventoryService(repo,partValidator,spiedValidator);
        Mockito.when(repo.getAutoProductId()).thenReturn(0);
        Product product = new Product(0,"Electric Car", 273001.99, 2, 1, 5, FXCollections.observableArrayList());
        Mockito.doNothing().when(repo).addProduct(product);

        assertDoesNotThrow(() -> service.addProduct("Electric Car", 273001.99, 2, 1, 5, FXCollections.observableArrayList()));

        Mockito.verify(spiedValidator, times(1)).validate(product);
        Mockito.verify(repo, times(1)).addProduct(product);
    }

    @Test
    @DisplayName("Test add product where price very negative and name is too short")
    void test_addProduct_nonvalid_nameTooShort_priceNegativeNameTooShort(){
        ProductValidator spiedValidator = spy(productValidator);
        service = new InventoryService(repo,partValidator,spiedValidator);
        Product product = new Product(0,"AA", -700, 2, 1, 5, FXCollections.observableArrayList());
        ServiceException exceptionThrown = assertThrows(ServiceException.class,() -> service.addProduct("AA", -700, 2, 1, 5, FXCollections.observableArrayList()));


        assertEquals(exceptionThrown.getMessage().trim(), "The price must be greater than $0. Product price must be greater than cost of parts. Name length has to be between 3 and 30".trim());
        Mockito.verify(repo, times(0)).addProduct(product);
        Mockito.verify(spiedValidator, times(1)).validate(product);
    }

    @Test
    @DisplayName("Test add product where name too long and negative price")
    void test_addProduct_nonvalid_nameTooLong_priceNegative(){
        ProductValidator spiedValidator = spy(productValidator);
        service = new InventoryService(repo,partValidator,spiedValidator);
        Product product = new Product(0, "1234567890123456789012345678901", -1, 2, 1, 5, FXCollections.observableArrayList());

        ServiceException exceptionThrown = assertThrows(ServiceException.class,() -> service.addProduct("1234567890123456789012345678901", -1, 2, 1, 5, FXCollections.observableArrayList()));

        assertEquals(exceptionThrown.getMessage().trim(), "The price must be greater than $0. Product price must be greater than cost of parts. Name length has to be between 3 and 30".trim());
        Mockito.verify(repo, times(0)).addProduct(product);
        Mockito.verify(spiedValidator, times(1)).validate(product);
    }
}
