package inventory.service;

import inventory.exceptions.ServiceException;
import inventory.model.Inventory;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.validator.PartValidator;
import inventory.validator.ProductValidator;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.io.*;
import java.util.StringTokenizer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class TestClass_Service_Int_Repo {
    private String filename = "testFile.txt";
    InventoryService service;
    PartValidator partValidator;
    ProductValidator productValidator;
    InventoryRepository repository;


    @BeforeEach
    void setUp() {
        partValidator = new PartValidator();
        productValidator = new ProductValidator();
        repository = new InventoryRepository(filename);
    }

    @Test
    @DisplayName("Test add product valid")
    void test_addProduct_Valid() {
        ProductValidator spiedValidator = spy(productValidator);
        Product product = new Product(0,"Electric Car", 273001.99, 2, 1, 5, FXCollections.observableArrayList());
        Inventory mockedInventory = mock(Inventory.class);
        Mockito.doNothing().when(mockedInventory).addProduct(product);
        Mockito.when(mockedInventory.getAllParts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(mockedInventory.getProducts()).thenReturn(FXCollections.observableArrayList(product));
        Mockito.when(mockedInventory.getAutoPartId()).thenReturn(0);
        repository.setInventory(mockedInventory);
        service = new InventoryService(repository,partValidator,spiedValidator);



        assertDoesNotThrow(() -> service.addProduct("Electric Car", 273001.99, 2, 1, 5, FXCollections.observableArrayList()));

        Mockito.verify(spiedValidator, times(1)).validate(product);


        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        Integer count = 0;
        try(BufferedReader br = new BufferedReader(new FileReader(file)); ) {
            String line = null;
            while((line=br.readLine())!=null){
                if (line.equals("")) continue;

                StringTokenizer st=new StringTokenizer(line, ",");
                String type=st.nextToken();
                if (type.equals("P")) {
                    count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(1, count);
    }

    @Test
    @DisplayName("Test add product nonvalid where price very negative and name is too short")
    void test_addProduct_nonvalid_nameTooShort_priceNegativeNameTooShort(){
        ProductValidator spiedValidator = spy(productValidator);
        Product product = new Product(0,"AA", -700, 2, 1, 5, FXCollections.observableArrayList());
        Inventory mockedInventory = mock(Inventory.class);
        Mockito.doNothing().when(mockedInventory).addProduct(product);
        Mockito.when(mockedInventory.getProducts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(mockedInventory.getAutoPartId()).thenReturn(0);
        repository.setInventory(mockedInventory);
        service = new InventoryService(repository,partValidator,spiedValidator);


        Integer countBeforeAdding = service.getAllProducts().size();

        ServiceException exceptionThrown = assertThrows(ServiceException.class,() -> service.addProduct("AA", -700, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();

        Mockito.verify(spiedValidator, times(1)).validate(product);

        assertEquals(countAfterAdding , countBeforeAdding);

        assertEquals(exceptionThrown.getMessage().trim(), "The price must be greater than $0. Product price must be greater than cost of parts. Name length has to be between 3 and 30".trim());
    }

    @Test
    @DisplayName("Test add product nonvalid name too long and negative price")
    void test_addProduct_nonvalid_nameTooLong_priceNegative(){
        ProductValidator spiedValidator = spy(productValidator);
        Product product = new Product(0,"1234567890123456789012345678901", -1, 2, 1, 5, FXCollections.observableArrayList());
        Inventory mockedInventory = mock(Inventory.class);
        Mockito.doNothing().when(mockedInventory).addProduct(product);
        Mockito.when(mockedInventory.getProducts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(mockedInventory.getAutoPartId()).thenReturn(0);
        repository.setInventory(mockedInventory);
        service = new InventoryService(repository,partValidator,spiedValidator);

        Integer countBeforeAdding = service.getAllProducts().size();

        ServiceException exceptionThrown = assertThrows(ServiceException.class,() -> service.addProduct("1234567890123456789012345678901", -1, 2, 1, 5, FXCollections.observableArrayList()));
        Integer countAfterAdding = service.getAllProducts().size();

        Mockito.verify(spiedValidator, times(1)).validate(product);

        assertEquals(countAfterAdding , countBeforeAdding);

        assertEquals(exceptionThrown.getMessage().trim(), "The price must be greater than $0. Product price must be greater than cost of parts. Name length has to be between 3 and 30".trim());
    }

    @AfterEach
    void tearDown() {
        try {
            ClassLoader classLoader = InventoryRepository.class.getClassLoader();
            File file = new File(classLoader.getResource(filename).getFile());
            new PrintWriter(file).close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
