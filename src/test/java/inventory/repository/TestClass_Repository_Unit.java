package inventory.repository;

import inventory.exceptions.ServiceException;
import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import inventory.service.InventoryService;
import inventory.validator.PartValidator;
import inventory.validator.ProductValidator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.*;
import java.util.StringTokenizer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TestClass_Repository_Unit {
    private String filename = "testFile.txt";
    InventoryRepository repository;

    @BeforeEach
    void setUp() {
        repository = new InventoryRepository(filename);
    }

    @DisplayName("Test add product")
    @Test
    void test_addProduct_Valid() {
        Product product = new Product(0,"Electric Car", 273001.99, 2, 1, 5, FXCollections.observableArrayList());
        Inventory mockedInventory = mock(Inventory.class);
        Mockito.doNothing().when(mockedInventory).addProduct(product);
        Mockito.when(mockedInventory.getAllParts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(mockedInventory.getProducts()).thenReturn(FXCollections.observableArrayList(product));
        repository.setInventory(mockedInventory);

        repository.addProduct(product);

        Mockito.verify(mockedInventory, times(1)).addProduct(product);
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        Integer count = 0;
        try(BufferedReader br = new BufferedReader(new FileReader(file)); ) {
            String line = null;
            while((line=br.readLine())!=null){
                if (line.equals("")) continue;

                StringTokenizer st=new StringTokenizer(line, ",");
                String type=st.nextToken();
                if (type.equals("P")) {
                    count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(count, 1);
    }

    @DisplayName("Test add part")
    @Test
    void test_addPart_Valid() {
        Part part = new InhousePart(0, "part", 10.0, 10, 1, 100, 3);
        Inventory mockedInventory = mock(Inventory.class);
        Mockito.doNothing().when(mockedInventory).addPart(part);
        Mockito.when(mockedInventory.getAllParts()).thenReturn(FXCollections.observableArrayList(part));
        Mockito.when(mockedInventory.getProducts()).thenReturn(FXCollections.observableArrayList());
        repository.setInventory(mockedInventory);

        repository.addPart(part);

        Mockito.verify(mockedInventory, times(1)).addPart(part);
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        Integer count = 0;
        try(BufferedReader br = new BufferedReader(new FileReader(file)); ) {
            String line = null;
            while((line=br.readLine())!=null){
                if (line.equals("")) continue;

                StringTokenizer st=new StringTokenizer(line, ",");
                String type=st.nextToken();
                if (type.equals("I") || type.equals("O")) {
                    count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(count, 1);
    }


    @AfterEach
    void tearDown() {
        try {
            ClassLoader classLoader = InventoryRepository.class.getClassLoader();
            File file = new File(classLoader.getResource(filename).getFile());
            new PrintWriter(file).close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
