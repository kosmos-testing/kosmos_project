package inventory.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestClass_Inventory_Unit {
    @Test
    @DisplayName("Test returns null when search item is null ")
    void test_searchItem_null(){
        Inventory inventory = new Inventory();
        String searchItem = null;

        Product product = inventory.lookupProduct(searchItem);

        assertNull(product);
    }

    @Test
    @DisplayName("Test returns an empty product when products is empty")
    void test_products_empty(){
        Inventory inventory = new Inventory();
        String searchItem = "surub";

        Product product = inventory.lookupProduct(searchItem);
        Product expectedProduct =  new Product(0, null, 0.0, 0, 0, 0, null);

        assertEquals(product.getProductId(), expectedProduct.getProductId());
    }

    @Test
    @DisplayName("Test finds product when it's the first one in products")
    void test_searchItem_first_in_products(){
        Inventory inventory = new Inventory();
        String searchItem = "15";
        Product expectedProduct =  new Product(15, "surub15", 0.0, 0, 0, 0, null);
        inventory.addProduct(expectedProduct);

        Product product = inventory.lookupProduct(searchItem);

        assertEquals(product.getProductId(), expectedProduct.getProductId());
    }

    @Test
    @DisplayName("Test finds product when it's the second one in products")
    void test_searchItem_second_in_products(){
        Inventory inventory = new Inventory();
        String searchItem = "surub";
        Product expectedProduct =  new Product(15, "surub15", 0.0, 0, 0, 0, null);
        inventory.addProduct(new Product(11, "ceva", 0.0, 0, 0, 0, null));
        inventory.addProduct(expectedProduct);

        Product product = inventory.lookupProduct(searchItem);

        assertEquals(product.getProductId(), expectedProduct.getProductId());
    }

    @Test
    @DisplayName("Test does not find product when product is not in products")
    void test_searchItem_not_in_products(){
        Inventory inventory = new Inventory();
        String searchItem = "surub";
        inventory.addProduct(new Product(11, "ceva", 0.0, 0, 0, 0, null));
        inventory.addProduct(new Product(15, "ceva", 0.0, 0, 0, 0, null));

        Product product = inventory.lookupProduct(searchItem);

        assertNull(product);
    }

    @Test
    @DisplayName("Test adds a product to the inventory")
    void test_addProduct_valid() {
        Inventory inventory = new Inventory();
        Product product = new Product(15, "surub15", 0.0, 0, 0, 0, null);

        inventory.addProduct(product);

        assertTrue(inventory.getProducts().contains(product));
    }

}
